const accountSid = 'AC0385323977b0489060c0f1a2bf18e645'; 
const authToken = '81207e6b8e0db22dfa53346cc58d3d70'; 
const client = require('twilio')(accountSid, authToken); 
const request = require('request');
const fs = require('fs');
const { dataFxtm } = require('./test-data');

console.log('FXTM Running...');
 
const sendNotification = (message) => {
    client.messages
    .create({
        body: message,
        from: 'whatsapp:+14155238886',
        to: 'whatsapp:+6281289464536'
    }) 
    .then(message => console.log(message.sid))
    .done();
};

var activeOrder = false;

setInterval(function(){
    request({
        url: "https://my.forextime.com/api/js/api/direct/investment-program/follower-trade-statistics?acc_no=36488034&closed_orders_page=1&lang=en&open_orders_page=1",
        method: "GET",
        headers: {
            'cookie': '_cq_duid=1.1625289699.K7beQA4tV14KRNHa; _gcl_au=1.1.216695226.1625289753; _rdt_uuid=1625289754672.93c8cd11-4ffe-46fc-8bed-f4fac16d03d2; _fbp=fb.1.1625289758810.1234280194; _fz_uniq=6351475629841147453; _fz_fvdt=1625289789; __zlcmid=14tk9PhmrJQCbR9; __adroll_fpc=2c05909a623dd32fee03ec1663d824bc-1625290244961; _userai=1; _redirect=2; _gcl_dc=GCL.1626402907.CjgKEAjwub-HBhC6qbjT-9jLqTcSJADbLj5goTwjtsWqPKxqBlM4Git365oELwBpIbM5mAClSqtVbvD_BwE; mf_user=d5a4a114d42c186d8872ac0ec0e83457|; __ar_v4=PCWTQSVOONBGHDKWXXOII4%3A20210818%3A1%7CGYRFEXR6HRCHPPO3ZEO4KN%3A20210818%3A2%7C2WCPDNFWKFB33NZUH5KMRN%3A20210818%3A2%7CLG2CJNIN6NGJBLAE5MBIFQ%3A20210818%3A1; company=global; country=ID; language=en; origin=https://www.forextime.com; show_region=ftg; ab.storage.userId.1cec6b00-9b66-4442-be56-486e46ed4cc4=%7B%22g%22%3A%2263285973%22%2C%22c%22%3A1630378288604%2C%22l%22%3A1630378288604%7D; __atuvc=2%7C33%2C0%7C34%2C3%7C35; redirect=myfxtm:accounts/open; redirect_auth=myfxtm:accounts/open; redirect_login=myfxtm:accounts/open; currentWWWUrl=https://www.forextime.com/login; topUrl=https://www.forextime.com/login; __zlcprivacy=1; SSESS7019a16d50ed462075863a47ded1bd69=b5np-E7Ge4lY2vYK40-JAoHBkNTMbGD4v3cH3V9xB5U; _gid=GA1.2.1040231292.1631503913; _pk_ref.7.965d=%5B%22%22%2C%22%22%2C1631503977%2C%22https%3A%2F%2Fwww.forextime.com%2F%22%5D; amp_4a7747=D5jzogP2H_eT6zI35ft7f6...1ffeje30c.1ffeje7fm.21.0.21; gotoPath=%2Frobots.txt; sid=45d0d8463045cc17b071f5270a779474d4868c09; _pk_ref.7.763d=%5B%22%22%2C%22%22%2C1631511930%2C%22https%3A%2F%2Fwww.forextime.com%2F%22%5D; _pk_ses.7.763d=1; _ga_MX7217M0KX=GS1.1.1631511927.73.1.1631511969.18; _uetsid=236f3fe0144311ecab3f47ea6e109d07; _uetvid=abbc0b40dbbe11eb8e27b112d29ff453; ab.storage.sessionId.1cec6b00-9b66-4442-be56-486e46ed4cc4=%7B%22g%22%3A%22c134e993-a47c-5556-8ec5-a3d73afa7020%22%2C%22e%22%3A1631513769584%2C%22c%22%3A1631511928680%2C%22l%22%3A1631511969584%7D; _pk_id.7.965d=9719e431b96486d0.1625290167.33.1631511970.1631503977.; _pk_id.7.763d=b11bceeb480a9307.1625290243.61.1631511970.1631509108.; _ga=GA1.1.823943054.1625289753; amp_4470ac=RJQHdLZiqpEUG1dPTROMt-...1ffer0oa6.1ffer219v.7g.1.7h; XSRF-TOKEN=e54bcd2d5cd42016d1c2d9139f6b9c1d82f2dbe7'
        },
        timeout: 30000,
        followRedirect: true,
        maxRedirects: 10
    },function(error, response, body){
        if(!error && response.statusCode == 200){
            console.log('Success!');

            var responseData = JSON.parse(body);
            var data = responseData.data;
            // Testing Data
            // var data = dataFxtm;
            var time = new Date();
            var day = time.getDate();
            var month = time.getMonth();
            var year = time.getFullYear();
            var date = year + "-" + month + "-" + day
            var logsPath = './logs/fxtm';
            
            if (!fs.existsSync(logsPath)){
                fs.mkdirSync(logsPath);
            }

            fs.appendFile("logs/fxtm/log-" + date + ".txt", JSON.stringify(responseData) + "\n\n==========\n\n", function(err) {
                if(err) {
                    return console.log(err);
                }
            });
            if (responseData.success == 1) {
                if (data.direct.open_orders.items.length > 0) {
                    activeOrder = true;
                    var openOrders = data.direct.open_orders.items;
                    var floatingPnl = data.direct.trade_result.toFixed(2);
                    openOrders.forEach(order => {
                        var orderVolume = (order.volume * 1000) / data.direct.balance;
                        orderVolume = orderVolume.toFixed(3);
                        var message = order.order_type + " " + order.symbol + " " + orderVolume + "/1k (PnL: " + floatingPnl + ")"; 
                        console.log(message);
                        sendNotification(message);
                    });
                } else {
                    if (activeOrder) {
                        activeOrder = false;
                        var message = "CLOSE"; 
                        console.log(message);
                        sendNotification(message);
                    } else {
                        console.log('No open order.');
                    }
                }
            }
        }else{
            console.log('error' + response.statusCode);
        }
    });
}, 300000);