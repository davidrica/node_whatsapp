var dataFxtm = {
    "direct": {
        "balance": 3137.36,
        "equity": 3135.04,
        "trade_result": -2.3200000000002,
        "open_orders": {
          "page": 1,
          "page_count": 1,
          "page_size": 5,
          "total_items": 1,
          "items": [
            {
              "ticket": 0,
              "order_type": "BUY",
              "volume": 0.1705,
              "symbol": "EURJPY",
              "open_time": "1970-01-01 02:00:00",
              "open_price": 0,
              "stop_loss_level": 0,
              "take_profit_level": 0
            }
          ]
        },
        "closed_orders": {
          "page": 1,
          "page_count": 5,
          "page_size": 10,
          "total_items": 50,
          "items": [
            {
              "ticket": 2234457334,
              "order_type": "SELL",
              "volume": 0.0227,
              "symbol": "CADJPY",
              "open_time": "2021-09-07 18:17:12",
              "open_price": 87.206,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-08 04:37:20",
              "close_price": 87.299,
              "commission": -0.0625,
              "storage": -0.0844,
              "profit": -1.9143
            },
            {
              "ticket": 2234457580,
              "order_type": "SELL",
              "volume": 0.1136,
              "symbol": "CADJPY",
              "open_time": "2021-09-07 18:17:30",
              "open_price": 87.204,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-08 04:37:16",
              "close_price": 87.301,
              "commission": -0.324,
              "storage": -0.4223,
              "profit": -9.992
            },
            {
              "ticket": 2234433753,
              "order_type": "SELL",
              "volume": 0.0227,
              "symbol": "GBPUSD",
              "open_time": "2021-09-07 14:55:57",
              "open_price": 1.37687,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-07 15:43:27",
              "close_price": 1.37877,
              "commission": -0.1136,
              "storage": 0,
              "profit": -4.313
            },
            {
              "ticket": 2234337830,
              "order_type": "BUY",
              "volume": 0.1478,
              "symbol": "USDCHF",
              "open_time": "2021-09-06 03:34:55",
              "open_price": 0.91446,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-06 12:31:22",
              "close_price": 0.91635,
              "commission": -0.5343,
              "storage": 0,
              "profit": 30.4842
            },
            {
              "ticket": 2234337833,
              "order_type": "BUY",
              "volume": 0.4547,
              "symbol": "USDCHF",
              "open_time": "2021-09-06 03:34:55",
              "open_price": 0.91446,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-06 12:31:11",
              "close_price": 0.9164,
              "commission": -1.6372,
              "storage": 0,
              "profit": 96.2591
            },
            {
              "ticket": 2234336982,
              "order_type": "BUY",
              "volume": 0.4623,
              "symbol": "USDCHF",
              "open_time": "2021-09-06 03:09:25",
              "open_price": 0.91432,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-06 03:34:16",
              "close_price": 0.91445,
              "commission": -1.6646,
              "storage": 0,
              "profit": 6.5721
            },
            {
              "ticket": 2234336835,
              "order_type": "BUY",
              "volume": 0.1502,
              "symbol": "USDCHF",
              "open_time": "2021-09-06 03:08:25",
              "open_price": 0.91426,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-06 03:34:16",
              "close_price": 0.91445,
              "commission": -0.5433,
              "storage": 0,
              "profit": 3.1208
            },
            {
              "ticket": 2234305529,
              "order_type": "SELL",
              "volume": 0.0327,
              "symbol": "USDJPY",
              "open_time": "2021-09-03 17:03:30",
              "open_price": 109.677,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-03 17:56:15",
              "close_price": 109.648,
              "commission": -0.1131,
              "storage": 0,
              "profit": 0.8649
            },
            {
              "ticket": 2234305529,
              "order_type": "SELL",
              "volume": 0.003,
              "symbol": "USDJPY",
              "open_time": "2021-09-03 17:03:30",
              "open_price": 109.677,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-03 17:56:10",
              "close_price": 109.653,
              "commission": -0.0119,
              "storage": 0,
              "profit": 0.0657
            },
            {
              "ticket": 2234305529,
              "order_type": "SELL",
              "volume": 0.003,
              "symbol": "USDJPY",
              "open_time": "2021-09-03 17:03:30",
              "open_price": 109.677,
              "stop_loss_level": null,
              "take_profit_level": null,
              "close_time": "2021-09-03 17:56:04",
              "close_price": 109.654,
              "commission": -0.0119,
              "storage": 0,
              "profit": 0.0629
            }
          ]
        }
      },
      "notifications": [],
      "client_info": {
        "name": "David",
        "firstName": "David",
        "clientListIds": [
          10
        ],
        "lastName": "Ricardo",
        "is_name_empty": false,
        "companyName": null,
        "id": "63285973",
        "clientCategory": 0,
        "branch_id": "1",
        "registrationDate": "2021-07-03 08:30:37",
        "dateOfBirth": "1994-04-12",
        "residentialAddress": "H DOMANG NO 16 008 002 KELAPA DUA ",
        "country": "ID",
        "asm": "Grace Lie",
        "at_passed": "0",
        "at_failed_first_attempt": false,
        "raf": "e53f5f25",
        "is_slave_referral": false,
        "canRefer": true,
        "allowedToChangePartnerId": false,
        "has_opened_accounts": true,
        "agent_id": null,
        "title": "David Ricardo",
        "display_questionnaire": true,
        "gold_terms_agreed": null,
        "is_terms_agreed": true,
        "ib_page": false,
        "client_locked_by_source_wealth": false,
        "_embedded": {
          "email": {
            "value": "davidlikaldo@gmail.com",
            "verified": true,
            "_embedded": {
              "actions": {
                "list": [
                  "verify"
                ]
              }
            }
          },
          "mobile_phone": {
            "value": "6281289464536",
            "verified": false,
            "_embedded": {
              "actions": {
                "list": [
                  "verify"
                ]
              }
            }
          },
          "client_additional_emails": [],
          "schema": {
            "client_additional_emails": {
              "max_allowed": 2
            }
          }
        },
        "start_trading": [],
        "inbox_count": {
          "unread_messages": 15
        },
        "company_id": "3",
        "surname": "Ricardo",
        "status_id": "12",
        "swap_free": "1",
        "email": "davidlikaldo@gmail.com",
        "mobile_phone": "6281289464536",
        "investor_workflow_hours": false
      }
};

var dataOcta = {"rows":[],"next":null};

var dataOctaClosed = {"rows":[{"icon":"buy","text":"","date":"2021-09-14","openTime":"17:02","closeTime":"17:03","duration":"58s","volume":0.01,"symbol":"GBPUSD","profit":0.17000000000000001,"isTrade":true,"currency":"USD"},{"icon":"buy","text":"","date":"2021-09-14","openTime":"16:53","closeTime":"16:55","duration":"01m 51s","volume":0.01,"symbol":"GBPUSD","profit":0.11,"isTrade":true,"currency":"USD"},{"icon":"buy","text":"","date":"2021-09-14","openTime":"16:46","closeTime":"16:46","duration":"48s","volume":0.01,"symbol":"XAUUSD","profit":1.26,"isTrade":true,"currency":"USD"},{"icon":"buy","text":"","date":"2021-09-14","openTime":"15:45","closeTime":"15:46","duration":"01m 23s","volume":0.01,"symbol":"GBPUSD","profit":0.68999999999999995,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-14","openTime":"12:25","closeTime":"12:36","duration":"11m 12s","volume":0.01,"symbol":"GBPUSD","profit":0.58999999999999997,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-13","openTime":"18:31","closeTime":"18:58","duration":"26m 51s","volume":0.01,"symbol":"GBPUSD","profit":0.20999999999999999,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-13","openTime":"17:00","closeTime":"17:06","duration":"06m 27s","volume":0.01,"symbol":"GBPUSD","profit":0.34999999999999998,"isTrade":true,"currency":"USD"},{"icon":"buy","text":"","date":"2021-09-13","openTime":"16:13","closeTime":"16:22","duration":"08m 57s","volume":0.01,"symbol":"GBPUSD","profit":0.42999999999999999,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-13","openTime":"15:55","closeTime":"15:57","duration":"01m 57s","volume":0.01,"symbol":"GBPUSD","profit":0.14999999999999999,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-13","openTime":"15:13","closeTime":"15:18","duration":"05m 13s","volume":0.01,"symbol":"GBPUSD","profit":0.28000000000000003,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-10","openTime":"09:58","closeTime":"18:07","duration":"08h 09m 55s","volume":0.01,"symbol":"GBPUSD","profit":0.089999999999999997,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-10","openTime":"14:05","closeTime":"16:16","duration":"02h 10m 50s","volume":0.01,"symbol":"GBPUSD","profit":0.54000000000000004,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-10","openTime":"10:46","closeTime":"13:41","duration":"02h 55m 37s","volume":0.01,"symbol":"GBPUSD","profit":0.19,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-09","openTime":"18:00","closeTime":"18:05","duration":"04m 51s","volume":0.01,"symbol":"GBPUSD","profit":0.20000000000000001,"isTrade":true,"currency":"USD"},{"icon":"sell","text":"","date":"2021-09-09","openTime":"15:32","closeTime":"16:03","duration":"30m 19s","volume":0.01,"symbol":"GBPUSD","profit":0.29999999999999999,"isTrade":true,"currency":"USD"}],"next":"\/copy-trade\/master\/8958617\/history\/closed\/2\/"};

exports.dataFxtm = dataFxtm;
exports.dataOctaClosed = dataOctaClosed;