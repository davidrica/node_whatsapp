const accountSid = 'AC0385323977b0489060c0f1a2bf18e645'; 
const authToken = '81207e6b8e0db22dfa53346cc58d3d70'; 
const client = require('twilio')(accountSid, authToken); 
const request = require('request');
const fs = require('fs');
const { dataOctaClosed } = require('./test-data');

console.log('Octa FX Running...');
 
const sendNotification = (message) => {
    client.messages
    .create({
        body: message,
        from: 'whatsapp:+14155238886',
        to: 'whatsapp:+6281289464536'
    }) 
    .then(message => console.log(message.sid))
    .done();
};

var activeOrder = false;

setInterval(function(){
    request({
        url: "https://www.octafx.com/copy-trade/master/8958617/history/open/0/",
        method: "GET",
        timeout: 30000,
        followRedirect: true,
        maxRedirects: 10
    },function(error, response, body){
        if(!error && response.statusCode == 200){
            console.log('Success!');

            var data = JSON.parse(body);
            // Testing Data
            // var data = dataOctaClosed;
            var time = new Date();
            var day = time.getDate();
            var month = time.getMonth();
            var year = time.getFullYear();
            var date = year + "-" + month + "-" + day
            var logsPath = './logs/octa';
            
            if (!fs.existsSync(logsPath)){
                fs.mkdirSync(logsPath);
            }

            fs.appendFile("logs/octa/log-" + date + ".txt", JSON.stringify(data) + "\n\n==========\n\n", function(err) {
                if(err) {
                    return console.log(err);
                }
            });

            if (data.rows.length > 0) {
                activeOrder = true;
                var openOrders = data.rows;
                var message = "";
                openOrders.forEach((order, i) => {
                    // var floatingPnl = order.profit;
                    // var orderVolume = (order.volume * 1000) / data.direct.balance;
                    // var orderVolume = order.volume;
                    // var message = order.icon + " " + order.symbol + " " + orderVolume + "/1k (PnL: " + floatingPnl + ")"; 
                    message += i + 1 + '. ' + order.icon + " " + order.symbol + " " + order.volume + " (PnL: " + order.profit + ")\n"; 
                });
                console.log('message:\n', message);
                sendNotification(message);
            } else {
                if (activeOrder) {
                    activeOrder = false;
                    var message = "CLOSE"; 
                    console.log(message);
                    sendNotification(message);
                } else {
                    console.log('No open order.');
                }
            }
        }else{
            console.log('error' + response.statusCode);
        }
    });
}, 300000);